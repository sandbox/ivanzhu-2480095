<?php

function weixin_suite_settings($form, &$form_state) {
	$form = array();
	$form['weixin_app_id'] = array(
		'#type' => 'textfield',
		'#title' => t('App id'),
		'#default_value' => variable_get('weixin_app_id'),
		'#required' => true,
	);

	$form['weixin_app_key'] = array(
		'#type' => 'textfield',
		'#title' => t('App key'),
		'#default_value' => variable_get('weixin_app_key'),
		'#required' => true,
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('submit'),
	);

	return $form;
}

function weixin_suite_settings_submit($form, &$form_state) {

	variable_set('weixin_app_id', $form_state['values']['weixin_app_id']);
	variable_set('weixin_app_key', $form_state['values']['weixin_app_key']);
}